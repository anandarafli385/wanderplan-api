-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Dec 15, 2023 at 11:02 PM
-- Server version: 10.4.27-MariaDB
-- PHP Version: 7.4.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_wanderplan`
--

-- --------------------------------------------------------

--
-- Table structure for table `payment`
--

CREATE TABLE `payment` (
  `id` bigint(20) NOT NULL,
  `id_tour` bigint(20) NOT NULL,
  `id_user` bigint(20) NOT NULL,
  `email` varchar(255) NOT NULL,
  `start_date` date NOT NULL,
  `end_data` date NOT NULL,
  `member` int(11) NOT NULL,
  `method` enum('Shopeepay','Dana','Gopay','Bank Account') NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `end_date` datetime NOT NULL,
  `phone` int(15) NOT NULL,
  `status` enum('paid','not paid') NOT NULL DEFAULT 'not paid'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `payment_sequence`
--

CREATE TABLE `payment_sequence` (
  `next_val` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `payment_sequence`
--

INSERT INTO `payment_sequence` (`next_val`) VALUES
(1);

-- --------------------------------------------------------

--
-- Table structure for table `tour`
--

CREATE TABLE `tour` (
  `id` bigint(20) NOT NULL,
  `name` varchar(255) NOT NULL,
  `location` varchar(255) NOT NULL,
  `price` int(11) NOT NULL,
  `rate` float NOT NULL,
  `description` varchar(255) NOT NULL,
  `image` text NOT NULL,
  `type` enum('tour','hotel') NOT NULL DEFAULT 'tour',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `tour`
--

INSERT INTO `tour` (`id`, `name`, `location`, `price`, `rate`, `description`, `image`, `type`, `created_at`) VALUES
(12, 'Candi Borobudur', 'Magelang, Jawa tengah, Indonesia', 1000000, 5, 'Candi Borobudur (Jawa: ꦕꦟ꧀ꦝꦶꦧꦫꦧꦸꦝꦸꦂ, translit. Candhi Båråbudhur) adalah sebuah candi Buddha yang terletak di Borobudur, Magelang, Jawa Tengah, Indonesia...', 'img_1', 'tour', '2023-12-15 21:42:58'),
(13, 'Pantai Kuta', 'Bali, Indonesia', 500000, 4.5, 'Pantai yang indah dengan pasir putih', 'img_2', 'tour', '2023-12-15 21:42:56'),
(14, 'Taman Nasional Gunung Leuser', 'Gunung Leuser, Sumatra Utara, Indonesia', 800000, 4.8, 'Taman nasional dengan keanekaragaman hayati', 'img_3', 'tour', '2023-12-15 21:43:01'),
(15, 'Gunung Bromo', 'Bromo, Jawa Timur, Indonesia', 1200000, 4.7, 'Pegunungan yang menawarkan pemandangan spektakuler', 'img_4', 'tour', '2023-12-15 21:43:04'),
(16, 'Kota Tua Jakarta', 'Jakarta, Indonesia', 300000, 4.2, 'Kota tua dengan bangunan bersejarah', 'img_5', 'tour', '2023-12-15 21:42:53'),
(17, 'Raja Ampat', 'Raja Ampat, Papua Barat, Indonesia', 1500000, 5, 'Pulau eksotis dengan keindahan bawah laut', 'img_6', 'tour', '2023-12-15 21:43:07'),
(18, 'Danau Toba', 'Danau Toba, Sumatra Utara, Indonesia', 9000000, 4.9, 'Danau vulkanik dengan pemandangan gunung berapi', 'img_7', 'tour', '2023-12-15 21:43:14'),
(19, 'Luxury Beach Hotel', 'Bali, Indonesia', 3000000, 4.8, 'Hotel mewah di tepi pantai dengan pemandangan laut yang menakjubkan...', 'img_11', 'hotel', '2023-12-15 21:43:17'),
(20, 'Traditional Resort & Spa', 'Ubud, Bali, Indonesia', 2500000, 4.5, 'Resor bergaya tradisional dengan kolam renang pribadi di setiap villa...', 'img_22', 'hotel', '2023-12-15 21:43:19'),
(21, 'Five-Star Beachfront Resort', 'Nusa Dua, Bali, Indonesia', 4000000, 4.9, 'Hotel bintang lima dengan fasilitas lengkap dan akses langsung ke pantai...', 'img_33', 'hotel', '2023-12-15 21:43:22'),
(22, 'City Center Hotel', 'Jakarta, Indonesia', 1500000, 4.3, 'Hotel modern di pusat kota dengan akses mudah ke tempat wisata terkenal...', 'img_44', 'hotel', '2023-12-15 21:43:25'),
(23, 'Private Villa Retreat', 'Seminyak, Bali, Indonesia', 3500000, 4.7, 'Villa pribadi dengan taman tropis dan kolam renang eksklusif...', 'img_55', 'hotel', '2023-12-15 21:43:29');

-- --------------------------------------------------------

--
-- Table structure for table `tour_sequence`
--

CREATE TABLE `tour_sequence` (
  `next_val` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `tour_sequence`
--

INSERT INTO `tour_sequence` (`next_val`) VALUES
(24);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` bigint(20) NOT NULL,
  `name` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `name`, `username`, `email`, `password`, `created_at`) VALUES
(1, 'admin test', 'admin', 'admin@test.com', '12345678', '2023-12-14 20:31:45'),
(2, 'Ananda Rafli Juliansyah', 'jokosatoru', 'ananda@gmail.com', '12345678', '2023-12-15 17:54:34');

-- --------------------------------------------------------

--
-- Table structure for table `user_sequence`
--

CREATE TABLE `user_sequence` (
  `next_val` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `user_sequence`
--

INSERT INTO `user_sequence` (`next_val`) VALUES
(3);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `payment`
--
ALTER TABLE `payment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tour`
--
ALTER TABLE `tour`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `payment`
--
ALTER TABLE `payment`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tour`
--
ALTER TABLE `tour`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
