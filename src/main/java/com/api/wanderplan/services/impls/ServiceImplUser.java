package com.api.wanderplan.services.impls;

import com.api.wanderplan.services.ServiceUser;
import com.api.wanderplan.models.Insert.InsertLogin;
import com.api.wanderplan.models.Insert.InsertUser;
import com.api.wanderplan.models.MessageModel;
import com.api.wanderplan.models.ModelUser;
import com.api.wanderplan.repositories.RepositoryUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ServiceImplUser implements ServiceUser {
    @Autowired
    RepositoryUser repositoryUser;
    @Override
    public MessageModel login(InsertLogin insertLogin){
        MessageModel msg = new MessageModel();
        if(repositoryUser.existsByEmail(insertLogin.getEmail())){
            List<ModelUser> userFound = repositoryUser.findByEmail(insertLogin.getEmail());
            if(userFound.get(0).getEmail().equals(insertLogin.getEmail()) && userFound.get(0).getPassword().equals(insertLogin.getPassword())){
                ModelUser modelUser = new ModelUser();
                modelUser.setId(userFound.get(0).getId());
                modelUser.setEmail(userFound.get(0).getEmail());
                modelUser.setName(userFound.get(0).getName());
                modelUser.setPassword(userFound.get(0).getPassword());
                modelUser.setUsername(userFound.get(0).getUsername());
                msg.setStatus(true);
                msg.setData(modelUser);
                msg.setMessage("success login");
            }
        }else{
            msg.setStatus(false);
            msg.setData(null);
            msg.setMessage("failed login");
        }
        return msg;
    }

    @Override
    public MessageModel register(InsertUser insertUser){
        MessageModel msg = new MessageModel();
        if(!repositoryUser.existsByEmail(insertUser.getEmail())){
            ModelUser modelUser = new ModelUser();
            modelUser.setEmail(insertUser.getEmail());
            modelUser.setName(insertUser.getName());
            modelUser.setPassword(insertUser.getPassword());
            modelUser.setUsername(insertUser.getUsername());
            try{
                repositoryUser.save(modelUser);
                msg.setStatus(true);
                msg.setData(null);
                msg.setMessage("success register");
            }catch (Exception e){
                msg.setStatus(false);
                msg.setData(null);
                msg.setMessage(e.getMessage());
            }

        }else{
            msg.setStatus(false);
            msg.setData(null);
            msg.setMessage("user already exists");
        }
        return msg;
    }
}
