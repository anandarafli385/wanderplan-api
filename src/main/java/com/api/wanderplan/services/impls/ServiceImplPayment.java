package com.api.wanderplan.services.impls;

import com.api.wanderplan.models.Insert.InsertPayment;
import com.api.wanderplan.models.MessageModel;
import com.api.wanderplan.models.ModelPayment;
import com.api.wanderplan.repositories.RepositoryPayment;
import com.api.wanderplan.services.ServicePayment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ServiceImplPayment implements ServicePayment {

    @Autowired
    RepositoryPayment repositoryPayment;

    @Override
    public MessageModel getHistoryPayment(String email) {
        MessageModel msg = new MessageModel();
        List<ModelPayment> payment_found = repositoryPayment.findByEmail(email);
        msg.setStatus(true);
        msg.setData(payment_found);
        msg.setMessage("success");
        return msg;
    }

    @Override
    public MessageModel doTransaction(InsertPayment insertPayment) {
        MessageModel msg = new MessageModel();
        try {
            ModelPayment modelPayment = new ModelPayment();
            modelPayment.setTour_identifier(insertPayment.getTour_identifier());
            modelPayment.setUser_identifier(insertPayment.getUser_identifier());
            modelPayment.setEmail(insertPayment.getEmail());
            modelPayment.setPhone(insertPayment.getPhone());
            modelPayment.setStart_date(insertPayment.getStart_date());
            modelPayment.setEnd_date(insertPayment.getEnd_date());
            modelPayment.setMember(insertPayment.getMember());
            if (insertPayment.getMethod().equals(ModelPayment.MethodCategory.SHOPEEPAY.getValue()) || insertPayment.getMethod().equals(ModelPayment.MethodCategory.DANA.getValue()) || insertPayment.getMethod().equals(ModelPayment.MethodCategory.GOPAY.getValue()) || insertPayment.getMethod().equals(ModelPayment.MethodCategory.BANK_ACCOUNT.getValue())) {
                modelPayment.setMethod(insertPayment.getMethod());
                repositoryPayment.save(modelPayment).getId();
                msg.setStatus(true);
                msg.setData(null);
                msg.setMessage("success insert transaction");
            } else {
                msg.setStatus(false);
                msg.setData(null);
                msg.setMessage("choose the right transaction methode");
            }
        } catch (Exception e) {
            msg.setStatus(false);
            msg.setData(null);
            msg.setMessage(e.getMessage());
        }
        return msg;
    }

    @Override
    public MessageModel verifyTransaction(Long id) {
        MessageModel msg = new MessageModel();
        Optional<ModelPayment> payment_found = repositoryPayment.findById(id);
        ModelPayment modelPayment = payment_found.get();
        modelPayment.setStatus(ModelPayment.StatusCategory.DONE);
        msg.setStatus(true);
        msg.setData(payment_found);
        msg.setMessage("success");
        return msg;
    }
}
