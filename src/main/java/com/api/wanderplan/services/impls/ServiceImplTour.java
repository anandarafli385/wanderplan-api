package com.api.wanderplan.services.impls;

import com.api.wanderplan.models.Insert.InsertTour;
import com.api.wanderplan.models.MessageModel;
import com.api.wanderplan.models.ModelPayment;
import com.api.wanderplan.models.ModelTour;
import com.api.wanderplan.repositories.RepositoryTour;
import com.api.wanderplan.services.ServiceTour;
import com.fasterxml.jackson.databind.JsonNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class ServiceImplTour implements ServiceTour {
    @Autowired
    RepositoryTour repositoryTour;

    @Override
    public MessageModel getAllTour() {
        MessageModel msg = new MessageModel();
        List<ModelTour> tour_found = repositoryTour.findAll();
        List<Map<String,Object>> list_tour = new LinkedList<>();
        if(!tour_found.isEmpty()){
            for(ModelTour tour : tour_found) {
                Map<String,Object> map_tour = new LinkedHashMap<>();
                map_tour.put("id",tour.getId());
                map_tour.put("type",tour.getType());
                map_tour.put("name",tour.getName());
                map_tour.put("image",tour.getImage());
                list_tour.add(map_tour);
            }
        }
        msg.setStatus(true);
        msg.setData(list_tour);
        msg.setMessage("success");
        return msg;
    }

    @Override
    public MessageModel getTourDetail(Long id) {
        MessageModel msg = new MessageModel();
        if(repositoryTour.existsById(id)){
            Optional<ModelTour> tour_found = repositoryTour.findById(id);
            msg.setStatus(true);
            msg.setData(tour_found);
            msg.setMessage("success");
        }else{
            msg.setStatus(false);
            msg.setData("");
            msg.setMessage("not found");
        }
        return msg;
    }

    @Override
    public MessageModel insertTour(InsertTour insertTour) {
        MessageModel msg = new MessageModel();
//        try {
            ModelTour modelTour = new ModelTour();
            modelTour.setName(insertTour.getName());
            modelTour.setLocation(insertTour.getLocation());
            modelTour.setPrice(insertTour.getPrice());
            modelTour.setRate(insertTour.getRate());
            modelTour.setDescription(insertTour.getDescription());
            modelTour.setType(insertTour.getDescription());
            modelTour.setImage("image.png");
            if (insertTour.getType().equals(ModelTour.TourCategory.TOUR.getValue()) || insertTour.getType().equals(ModelTour.TourCategory.HOTEL.getValue())) {
                modelTour.setType(insertTour.getType());
                repositoryTour.save(modelTour);
                msg.setStatus(true);
                msg.setData(null);
                msg.setMessage("success insert transaction");
            } else {
                msg.setStatus(false);
                msg.setData(null);
                msg.setMessage("choose the right transaction methode");
            }
            msg.setStatus(true);
            msg.setData(null);
            msg.setMessage("success insert tour " + insertTour.getName());
//        } catch (Exception e) {
//            System.out.println(e);
//            msg.setStatus(false);
//            msg.setData(null);
//            msg.setMessage(e.getMessage());
//        }

        return msg;
    }
}
