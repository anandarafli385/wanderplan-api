package com.api.wanderplan.services;

import com.api.wanderplan.models.Insert.InsertPayment;
import com.api.wanderplan.models.MessageModel;

public interface ServicePayment {
    MessageModel getHistoryPayment(String email);
    MessageModel doTransaction(InsertPayment insertPayment);
    MessageModel verifyTransaction(Long id);
}
