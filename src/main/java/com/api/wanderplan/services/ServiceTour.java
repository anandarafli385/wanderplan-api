package com.api.wanderplan.services;

import com.api.wanderplan.models.Insert.InsertLogin;
import com.api.wanderplan.models.Insert.InsertTour;
import com.api.wanderplan.models.MessageModel;

public interface ServiceTour {
    MessageModel getAllTour();
    MessageModel getTourDetail(Long id);
    MessageModel insertTour(InsertTour insertTour);
}
