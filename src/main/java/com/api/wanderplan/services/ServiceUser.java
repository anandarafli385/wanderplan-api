package com.api.wanderplan.services;

import com.api.wanderplan.models.Insert.InsertLogin;
import com.api.wanderplan.models.Insert.InsertUser;
import com.api.wanderplan.models.MessageModel;

public interface ServiceUser {
    MessageModel login(InsertLogin insertLogin);
    MessageModel register(InsertUser insertUser);

}
