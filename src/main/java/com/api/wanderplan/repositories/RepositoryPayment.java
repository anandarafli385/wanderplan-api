package com.api.wanderplan.repositories;

import com.api.wanderplan.models.ModelPayment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface RepositoryPayment extends JpaRepository<ModelPayment,Long> {
    List<ModelPayment> findByEmail(String email);
    Optional<ModelPayment> findById(Long id);
}
