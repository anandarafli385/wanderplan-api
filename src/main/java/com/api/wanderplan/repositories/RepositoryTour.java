package com.api.wanderplan.repositories;

import com.api.wanderplan.models.ModelTour;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RepositoryTour extends JpaRepository<ModelTour,Long> {
}
