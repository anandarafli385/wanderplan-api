package com.api.wanderplan.controllers;

import com.api.wanderplan.models.Insert.InsertLogin;
import com.api.wanderplan.models.MessageModel;
import com.api.wanderplan.services.ServiceUser;
import com.api.wanderplan.models.Insert.InsertUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

@RestController
@CrossOrigin("*")
@RequestMapping(value = "/user")
public class ControllerUser {
    @Autowired
    ServiceUser serviceUser;

    ResponseEntity<?> response = null;

    @PostMapping(value = "/login",name = "login user")
    public ResponseEntity<?> login(@RequestBody InsertLogin insertLogin) throws IOException {
        System.out.println(insertLogin);
        MessageModel msg = serviceUser.login(insertLogin);
        if (msg.isStatus()) {
            response = new ResponseEntity(msg, HttpStatus.OK);
        }else {
            response = new ResponseEntity(msg, HttpStatus.UNAUTHORIZED);
        }
        return response;
    }

    @PostMapping(value = "/register",name = "register user")
    public ResponseEntity<?> register(@RequestBody InsertUser insertUser) throws IOException {
        System.out.println(insertUser);
        MessageModel msg = serviceUser.register(insertUser);
        if (msg.isStatus()) {
            response = new ResponseEntity(msg, HttpStatus.OK);
        }else {
            response = new ResponseEntity(msg, HttpStatus.SERVICE_UNAVAILABLE);
        }
        return response;
    }
}
