package com.api.wanderplan.controllers;

import com.api.wanderplan.models.Insert.InsertPayment;
import com.api.wanderplan.models.MessageModel;
import com.api.wanderplan.services.ServicePayment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

@RestController
@CrossOrigin("*")
@RequestMapping(value = "/payment")
public class ControllerPayment {
    @Autowired
    ServicePayment servicePayment;
    ResponseEntity<?> response = null;

    @PostMapping(value = "/get-history",name = "get history payment")
    public ResponseEntity<?> getHistoryPayment(String email) throws IOException {
        MessageModel msg = servicePayment.getHistoryPayment(email);
        if (msg.isStatus()) {
            response = new ResponseEntity(msg, HttpStatus.OK);
        }else {
            response = new ResponseEntity(msg, HttpStatus.UNAUTHORIZED);
        }
        return response;
    }

    @PostMapping(value = "/transaction",name = "insert payment transaction")
    public ResponseEntity<?> doTransaction(InsertPayment insertPayment) throws IOException {
        MessageModel msg = servicePayment.doTransaction(insertPayment);
        if (msg.isStatus()) {
            response = new ResponseEntity(msg, HttpStatus.OK);
        }else {
            response = new ResponseEntity(msg, HttpStatus.SERVICE_UNAVAILABLE);
        }
        return response;
    }

    @PostMapping(value = "/verify-transaction",name = "insert payment transaction")
    public ResponseEntity<?> verifyTransaction(Long id) throws IOException {
        MessageModel msg = servicePayment.verifyTransaction(id);
        if (msg.isStatus()) {
            response = new ResponseEntity(msg, HttpStatus.OK);
        }else {
            response = new ResponseEntity(msg, HttpStatus.NOT_FOUND);
        }
        return response;
    }
}
