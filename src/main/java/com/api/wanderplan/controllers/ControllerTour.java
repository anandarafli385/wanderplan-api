package com.api.wanderplan.controllers;

import com.api.wanderplan.models.Insert.InsertLogin;
import com.api.wanderplan.models.Insert.InsertTour;
import com.api.wanderplan.models.MessageModel;
import com.api.wanderplan.services.ServiceTour;
import com.api.wanderplan.services.ServiceUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Base64;

@RestController
@CrossOrigin("*")
@RequestMapping(value = "/tour")
public class ControllerTour {
    @Autowired
    ServiceTour serviceTour;
    ResponseEntity<?> response = null;

    @PostMapping(value = "/get-all-tour",name = "get all tour")
    public ResponseEntity<?> getAllTour() throws IOException {
        MessageModel msg = serviceTour.getAllTour();
        if (msg.isStatus()) {
            response = new ResponseEntity(msg, HttpStatus.OK);
        }else {
            response = new ResponseEntity(msg, HttpStatus.UNAUTHORIZED);
        }
        return response;
    }

    @PostMapping(value = "/get-tour-detail",name = "get all tour")
    public ResponseEntity<?> getTourDetail(Long id) throws IOException {
        MessageModel msg = serviceTour.getTourDetail(id);
        if (msg.isStatus()) {
            response = new ResponseEntity(msg, HttpStatus.OK);
        }else {
            response = new ResponseEntity(msg, HttpStatus.NOT_FOUND);
        }
        return response;
    }

    @PostMapping(value = "/insert-tour")
    public ResponseEntity<?> insertTour(@RequestBody InsertTour insertTour) throws IOException {
//        String base64Image = Base64.getEncoder().encodeToString(insertTour.getFile().getBytes());
        MessageModel msg = serviceTour.insertTour(insertTour);
        if (msg.isStatus()) {
            response = new ResponseEntity(msg, HttpStatus.OK);
        }else {
            response = new ResponseEntity(msg, HttpStatus.SERVICE_UNAVAILABLE);
        }
        return response;
    }
}
