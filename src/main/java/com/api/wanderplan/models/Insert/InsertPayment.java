package com.api.wanderplan.models.Insert;

import com.api.wanderplan.models.ModelPayment;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import java.util.Date;

@Data
public class InsertPayment extends ModelPayment {
    @JsonIgnore
    public Long getId()  {
        return super.getId();
    }
    @JsonIgnore
    public void setId(Long id) {
        super.setId(id);
    }
    @JsonIgnore
    public Date getCreated_at()  {
        return super.getCreated_at();
    }
    @JsonIgnore
    public void setCreated_at(Date created_at) {
        super.setCreated_at(created_at);
    }

    @JsonIgnore
    public StatusCategory getStatus()  {
        return super.getStatus();
    }
    @JsonIgnore
    public void setStatus(StatusCategory status) {
        super.setStatus(status);
    }
}
