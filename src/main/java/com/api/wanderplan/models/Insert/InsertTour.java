package com.api.wanderplan.models.Insert;

import com.api.wanderplan.models.ModelTour;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

import java.util.Date;

@Data
public class InsertTour extends ModelTour {
    @JsonIgnore
    public Long getId()  {
        return super.getId();
    }
    @JsonIgnore
    public void setId(Long id) {
        super.setId(id);
    }
    @JsonIgnore
    public Date getCreated_at()  {
        return super.getCreated_at();
    }
    @JsonIgnore
    public void setCreated_at(Date created_at) {
        super.setCreated_at(created_at);
    }

    @JsonIgnore
    public String getImage()  {
        return super.getImage();
    }
    @JsonIgnore
    public void setImage(String image) {
        super.setImage(image);
    }
}
