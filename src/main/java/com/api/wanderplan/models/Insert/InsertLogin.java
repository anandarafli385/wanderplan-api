package com.api.wanderplan.models.Insert;

import lombok.Data;

@Data
public class InsertLogin {
    private String email;
    private String password;
}
