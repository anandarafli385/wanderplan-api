package com.api.wanderplan.models;

import lombok.Data;

@Data
public class MessageModel {
    private boolean status;
    private String message;
    private Object data;
}
