package com.api.wanderplan.models;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import javax.persistence.*;
import java.sql.Blob;
import java.util.Date;

@Entity
@Data
@Table(name = "tour")
public class ModelTour {

    public enum TourCategory {
        TOUR("tour"),
        HOTEL("hotel");

        private final String value;

        TourCategory(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tour_seq")
    @SequenceGenerator(name = "tour_seq", sequenceName = "tour_sequence", allocationSize = 1)
    private Long id;

    @Column(name = "name",nullable = false)
    @Schema(description = "Tour Name", example = "Candi Borobudur [string]")
    private String name;

    @Column(name = "location",nullable = false)
    @Schema(description = "Tour Location", example = "Magelang, Jawa tengah, Indonesia [string]")
    private String location;

    @Column(name = "price",nullable = false)
    @Schema(description = "Tour Price", example = "100 [number|integer|double]")
    private Integer price;

    @Column(name = "rate",nullable = false)
    @Schema(description = "Tour Rate", example = "5.0 [float]")
    private Float rate;

    @Column(name = "description",nullable = false)
    @Schema(description = "Tour Description", example = "Candi Borobudur (Jawa: ꦕꦟ꧀ꦝꦶꦧꦫꦧꦸꦝꦸꦂ, translit. Candhi Båråbudhur) adalah sebuah candi Buddha yang terletak di Borobudur, Magelang, Jawa Tengah, Indonesia... [string]")
    private String description;

    @Column(name = "image",columnDefinition = "TEXT",nullable = false)
    @Schema(description = "Tour Image", example = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAABN4AAAHmCAYAAACs4ENBAAAABHNCSVQICAgIfA... [base64 String]")
    private String image;

    @Column(name = "type",nullable = false)
    @Schema(description = "Tour Category", example = "tour|hotel")
    private String type;

    @Column(name = "created_at", nullable = false, updatable = false, columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    private Date created_at;

    @PrePersist
    protected void onCreate() {
        if (created_at == null) {
            created_at = new Date();
        }
    }
}
