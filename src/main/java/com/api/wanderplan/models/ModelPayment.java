package com.api.wanderplan.models;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@Table(name = "payment")
public class ModelPayment {
     public enum MethodCategory {
        SHOPEEPAY("Shopeepay"),
        DANA("Dana"),
        GOPAY("Gopay"),
        BANK_ACCOUNT("Bank Account");

        private final String value;

        MethodCategory(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }

    public enum StatusCategory {
        DONE("paid"),
        NOT("not paid");

        private final String value;

        StatusCategory(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "payment_seq")
    @SequenceGenerator(name = "payment_seq", sequenceName = "payment_sequence", allocationSize = 1)
    private Long id;

    @Column(name = "id_tour",nullable = false)
    @Schema(description = "ID TOUR", example = "1 [integer]")
    private Long tour_identifier;

    @Column(name = "id_user",nullable = false)
    @Schema(description = "ID USER", example = "1 [integer]")
    private Long user_identifier;

    @Column(name = "email",nullable = false)
    @Schema(description = "Email", example = "JohnDoe@gmail.com [string]")
    private String email;

    @Column(name = "phone",nullable = false)
    @Schema(description = "Phone", example = "0819050412312 [integer]")
    private Integer phone;

    @Column(name = "start_date",nullable = false)
    @Schema(description = "Start Date", example = "2022-09-03 [date]")
    private Date start_date;

    @Column(name = "end_date",nullable = false)
    @Schema(description = "End Date", example = "2022-09-04 [date]")
    private Date end_date;

    @Column(name = "member",nullable = false)
    @Schema(description = "End Date", example = "2 [integer]")
    private Integer member;

    @Enumerated(EnumType.STRING)
    @Column(name = "method",nullable = false)
    @Schema(description = "Payment Method", example = "Shopeepay|Gopay|Dana|Bank Account")
    private MethodCategory method;

    @Enumerated(EnumType.STRING)
    @Column(name = "status",nullable = false)
    @Schema(description = "Payment Status", example = "paid|not paid")
    private StatusCategory status;

    @Column(name = "created_at", nullable = false, updatable = false, columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    private Date created_at;

    @PrePersist
    protected void onCreate() {
        if (created_at == null) {
            created_at = new Date();
        }
    }
}
