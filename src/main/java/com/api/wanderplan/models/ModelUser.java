package com.api.wanderplan.models;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Entity
@Data
@Table(name = "user")
public class ModelUser {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "user_seq")
    @SequenceGenerator(name = "user_seq", sequenceName = "user_sequence", allocationSize = 1)
    private Long id;

    @Column(name = "name",nullable = false)
    @Schema(description = "Full Name", example = "John Doe")
    private String name;

    @Column(name = "username",nullable = false)
    @Schema(description = "Username", example = "johndoe")
    private String username;

    @Column(name = "email",nullable = false,unique = true)
    @Schema(description = "Email", example = "JohnDoe@gmail.com")
    private String email;

    @Column(name = "password",nullable = false)
    @Schema(description = "password", example = "12345678")
    private String password;

    @Column(name = "created_at", nullable = false, updatable = false, columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    private Date created_at;

    @PrePersist
    protected void onCreate() {
        if (created_at == null) {
            created_at = new Date();
        }
    }
}
